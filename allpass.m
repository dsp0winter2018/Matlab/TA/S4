close all;clear all;clc
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Oct. 2017
% Freq Resp of a second order allpass filter
% Effect of phase distortion on a pulse/sine by an allpass filter
r=.95;
N=1024;
Fs=1;
w_pole=[ pi/4];
A1=conv([1 -r*exp(j*w_pole)],[1 -r*exp(-j*w_pole)]);
%
w_zero=w_pole;
B1=conv([1 -1/r*exp(j*w_zero)],[1 -1/r*exp(-j*w_zero)]);
%
W= linspace(-pi,pi,N);
H1=freqz(B1,A1,W);
figure(1);subplot(211);
plot(W,20*log10(abs(H1)),'b-');grid on
figure(1);subplot(212);
plot(W,phase(H1),'b-');grid on;
figure(1);subplot(211);title('Amplitude Response of a conjugate allpass pair');
subplot(212);title('Phase Response of a conjugate allpass pair');
%
%  Now filter a pulse through all pass filter with nonlinear phase
inp=[ones(1,20),zeros(1,10)];
inp=[inp,inp];
out=filter(B1,A1,inp);
figure(2);
plot(inp,'b.-');grid on ; hold on
plot(out,'r*-');
title('Input (BLUE) and Output of allpass filter (RED)');
%
n=1:64;
inp=sin(pi/8*n)+2*sin(pi/4*n);
out=filter(B1,A1,inp);
figure(3);
plot(inp,'b.-');grid on ; hold on
plot(out,'r*-');
title('Input (BLUE) and Output of allpass filter (RED)');

%



