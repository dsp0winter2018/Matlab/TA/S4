close all;clear all;clc
% DSP BSC course
% Demo program by Hamid Sheikhzadeh, Oct. 2017
% Min-phase/ Max-phase systems
%
r=.5; % Change to change pole or zero location
N=1024;
w=pi/4;
W= linspace(-pi+.01,pi-.01,N);
%
B_min=conv([1 -r*exp(j*w)],[1 -r*exp(-j*w)]);
B_min=B_min/abs(sum(B_min));
B_max=conv([1 -1/r*exp(j*w)],[1 -1/r*exp(-j*w)]);
B_max=B_max/abs(sum(B_max));
B_mix1=conv([1 -r*exp(j*w)],[1 -1/r*exp(-j*w)]);
B_mix1=B_mix1/abs(sum(B_mix1));
B_mix2=conv([1 -1/r*exp(j*w)],[1 -r*exp(-j*w)]);
B_mix2=B_mix2/abs(sum(B_mix2));
%
H_min=freqz(B_min,1,W);
H_max=freqz(B_max,1,W);
H_mix1=freqz(B_mix1,1,W);
H_mix2=freqz(B_mix2,1,W);
figure;
plot(W,20*log10(abs((H_min))),W,20*log10(abs((H_max))),W,20*log10(abs((H_mix1))),W,20*log10(abs((H_mix2))));grid on;
legend('Min Phase','Max Phase','mixed phase1','mixed phase2');
title('Mag. (dB) of 4 filters');
%
% To plot zero poles, uncomment these lines
%my_DTFT(B_min,1,N);my_DTFT(B_max,1,N);
%my_DTFT(B_mix1,1,N);my_DTFT(B_mix2,1,N);
figure;
R_min=roots(B_min);
R_max=roots(B_max);
R_mix1=roots(B_mix1);
R_mix2=roots(B_mix2);
%
subplot(221);polar(angle(R_min),abs(R_min),'o');grid on;title('Zeros of min-phase');
subplot(222);polar(angle(R_max),abs(R_max),'o');grid on;title('Zeros of max-phase');
subplot(223);polar(angle(R_mix1),abs(R_mix1),'o');grid on;title('Zeros of mixed-phase1');
subplot(224);polar(angle(R_mix2),abs(R_mix2),'o');grid on;title('Zeros of mixed-phase2');
%
figure
plot(W,phase(H_min),W,phase(H_max),W,phase(H_mix1),W,phase(H_mix2));grid on;
legend('Min Phase','Max Phase','mixed phase1','mixed phase2');
title('Phase Responses of 4 filters')
figure
Grd_min=grpdelay(B_min,1,W);
Grd_max=grpdelay(B_max,1,W);
Grd_mix1=grpdelay(B_mix1,1,W);
Grd_mix2=grpdelay(B_mix2,1,W);
plot(W,Grd_min,W,Grd_max,W,Grd_mix1,W,Grd_mix2);grid on
legend('Min Phase','Max Phase','mixed phase 1','mixed phase 2');
title('Group Delay of 4 filters');
%
figure
subplot(221);stem(B_min);grid on;title('Impulse Resp. of Min Phase');
subplot(222);stem(B_max);grid on;title('Impulse Resp. of Max Phase');
subplot(223);stem(B_mix1);grid on;title('Impulse Resp. of Mixed Phase1');
subplot(224);stem(B_mix2);grid on;title('Impulse Resp. of Mixed Phase2');
figure;
n=1:length(B_min);
plot(n,cumsum(abs(B_min).^2),n,cumsum(abs(B_max).^2),n,cumsum(abs(B_mix1).^2),n,cumsum(abs(B_mix2).^2));
legend('Min Phase','Max Phase','mixed phase1','mixed phase2');
title('Cumulative Energies of 4 filters');grid on


